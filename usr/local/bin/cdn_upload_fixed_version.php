#!/usr/bin/php
<?php
/**
    This script assumes that aws.phar SDK is already installed in a findable path for PHP 
    It also assumes that the file credentials with AWS_ACCESS_KEY and AWS_SECRET_ACCESS_KEY is set
    according to the guidelines at: http://docs.aws.amazon.com/aws-sdk-php/v2/guide/credentials.html 

    This script takes commandline parameters with the following format:
        key=value

    The following keys are available for customization:
        bucket - String representing the name of the bucket on the server
        region - String representing the region of the country/world where this will be stored
        localDir - String to be turned into an array if separated by a 'colon' between multiple local directories (all directories in list will be scanned for files)
        version - String representing the fix version which will marked on the uploads
        command - String representing the command to be given

    The following commands are available:
        CreateBucket
        UploadFiles <default>

    It will be up to the one running the script to ensure the proper parameters are placed,
    aws.phar is installed correctly, and that credentials are setup properly.
*/
require 'aws.phar';
// This script requires the installation of AWS SDK in the include path as set forth in php.ini
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class S3Controller 
{
    private $command = "UploadFiles";
    private $bucket = 'static.talentwise.jgunter.test'; // Sample
    private $rootDir = '/usr/local/depots/sample/cdn/static';
    private $localStaticDirs = '/'; // sample on dev machine, change if you are testing
    private $arrLocalStaticDirs = array();
    private $version = '4.3.2.0'; // sample for testing only
    private $region = 'us-west-2';
    private $S3 = null;
    private $CF = null;
    // For cloudfront, this allows the ability to send invalidations one at a time during delete and copy to S3 or in an array at the end of all S3 operations
    private $waitUntilValidation = false;

    public function __construct($argv, $argc) 
    {
        for ($i=1; $i < $argc; $i++) {
            list($key, $value) = explode('=', $argv[$i]);

            switch($key) {
                case 'wait'    : $this->waitUntilValidation = ($value === 'true'); break;
                case 'bucket'  : $this->bucket = $value; break;
                case 'region'  : $this->region = $value; break;
                case 'version' : $this->version = $value; break;
                case 'command' : $this->command = $value; break;
                case 'localDir': $this->localStaticDirs = $value; break;
                case 'cfid'    : $CFID = $value; break;
                case 'cfregion': $CFRegion = $value; break;
            }
        }

        if (!empty($this->localStaticDirs)) {
            $this->arrLocalStaticDirs = explode(":", $this->localStaticDirs);
        }

        // Instantiate the client.
        $this->S3 = new S3Client([
            'version'  => '2006-03-01',
            'region'   => $this->region,
            'validate' => ['required' => true]
        ]);

        // Instantiate CloudFront Controller
        $this->CF = new CFController();
        if (!empty($CFID)) {
            $this->CF->setCFID($CFID);
        }
        if (!empty($CFRegion)) {
            $this->CF->setRegion = $CFRegion;
        }

        if (!empty($this->command)) {
            $this->executeCommand();
        }

        if ($this->waitUntilValidation) {
            while (!$this->CF->checkInvalidations()) {
                echo "Waiting 60 more seconds for Invalidations to propagate to CDN\n";
                sleep(60);
            }
        }
        echo "Script finished successfully\n";
    }

    private function executeCommand()
    {
        switch($this->command) {
            case "UploadFiles" : $this->uploadFiles(); break;
            case "CreateBucket" : $this->createBucket(); break;
            default: break;
        }
    }

    private function createBucket()
    {
        echo "CreateBucket\n";
        if (!empty($this->bucket) && !empty($this->region)) {
            $this->S3->createBucket(array(
                 'Bucket' => $this->bucket,
                 'LocationConstraint' => $this->region,
            ));
        }
    }

    private function uploadFiles()
    {
        $Invalidations = array();
        if (!empty($this->arrLocalStaticDirs)) {
            foreach ($this->arrLocalStaticDirs as $localDir) {
                $localCompletePath = $this->rootDir.$localDir;
                $Directories = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($localCompletePath), RecursiveIteratorIterator::SELF_FIRST);
                foreach ($Directories as $filename => $object) {
                    $uploadFilePath = str_replace($this->rootDir, "", $filename);
                    if ($object->isFile()) {
                        $newFileInfo = $this->getFileReadyToTransfer($filename);
                        if ($this->doesFileExist("{$this->version}{$uploadFilePath}")) {
                            if ($this->deleteFile("{$this->version}{$uploadFilePath}")) {
                                echo "File /{$this->version}{$uploadFilePath} was deleted on the server\n";
                            } else {
                                echo "File /{$this->version}{$uploadFilePath} exists on the server but was not deleted before being overwritten\n";
                            }
                            array_push($Invalidations, "/{$this->version}{$uploadFilePath}");
                        }
                        $result = $this->S3->putObject(array(
                            'Bucket' => $this->bucket,
                            'Key'    => "{$this->version}{$uploadFilePath}",
                            'Body'   => fopen($newFileInfo['FileName'], 'r+'),
                            'ACL'    => 'public-read',
                            'ContentType' => $newFileInfo['ContentType'],
                        ));
                        if (!empty($result)) {
                            echo "File {$newFileInfo['FileName']} was uploaded to the server bucket: {$this->bucket} file: /{$this->version}{$uploadFilePath}\n";
                        } else {
                            // Placeholder for what we want to do when it errors out for real in the future.
                            echo "File {$newFileInfo['FileName']} failed to upload to the server bucket: {$this->bucket} file: /{$this->version}{$uploadFilePath}\n";
                        }
                    }
                }
                if (!empty($Invalidations)) {
                    $this->CF->setFilesToInvalidate($Invalidations);
                    $this->CF->clearCache();
                }
            }
        }
    }

    private function doesFileExist($filename)
    {
        $info = $this->S3->doesObjectExist($this->bucket, $filename);
        if ($info)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function deleteFile($filename)
    {
        $result = $this->S3->deleteObject(array(
            'Bucket' => $this->bucket,
            'Key' => $filename,
        ));

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    private function getFileReadyToTransfer($filename)
    {
        $FileInfo = array();
        if (!empty($filename)) {
            $FileInfo['FileName'] = $filename;
        }
        $FileInfo['ContentType'] = mime_content_type($filename);
        if (empty($FileInfo['ContentType'])) {
            $FileNameSplit = explode(".", $filename);
            $nFileNameSplit = count($FileNameSplit);
            if (!empty($FileNameSplit[$nFileNameSplit-1])) {
                switch($FileNameSplit[$nFileNameSplit-1]) {
                    case "woff":$FileInfo['ContentType'] = 'application/x-font-woff'; break;
                    case "jpg": $FileInfo['ContentType'] = 'image/jpeg'; break;
                    case "png": $FileInfo['ContentType'] = 'image/png'; break;
                    case "gif": $FileInfo['ContentType'] = 'image/gif'; break;
                    case "js":  $FileInfo['ContentType'] = 'text/javascript'; break;
                    case "css": $FileInfo['ContentType'] = 'text/css'; break;
                    default:    $FileInfo['ContentType'] = 'text/plain'; break;
                }
            }
        }
        return $FileInfo;
    }
}

class CFController
{
    const STATUS_IN_PROGRESS = "InProgress";
    const STATUS_COMPLETE = "Complete";

    private $CFID = 'E2TD5EDTTN1A9Q';  // Test CFID
    private $region = 'us-east-1'; // Not sure why this is us-east-1, but it is and it works, while other regions do not work.
    private $CF = null;
    private $Paths = array();  // test data
    private $epoch;
    private $Invalidations = array();

    public function __construct() 
    {
        $this->epoch = date('U');
        $this->CF = new CloudFrontClient([
            'version'  => 'latest',
            'region'   => $this->region,
            'validate' => ['required' => true]
        ]);
    }

    public function setCFID($CFID)
    {
        if (!empty($CFID)) {
            $this->CFID = $CFID;
        }
    }

    public function setRegion($region)
    {
        if (!empty($region)) {
            $this->region = $region;
        }
    }

    public function setFilesToInvalidate(array $Paths) 
    {
        echo "\nThe following CDN files needed to be repropagated:\n\n";
        print_r($Paths);
        if (!empty($Paths) && is_array($Paths)) {
            $this->Paths = $Paths;
        }
    }

    public function addInvalidationId($invalidation)
    {
        if (!empty($invalidation)) {
            array_push($this->Invalidations, $invalidation);
        }
    }

    public function clearCache()
    {
        $nPaths = count($this->Paths);
        if ($nPaths > 0) {
            $result = $this->CF->createInvalidation(array(
                'DistributionId' => $this->CFID,
                'Paths' => array(
                     'Quantity' => $nPaths,
                     'Items' => $this->Paths,
                ),
                'CallerReference' => "{$this->CFID}{$this->epoch}",
                'InvalidationBatch' => array(
                    'DistributionId' => $this->CFID,
                    'Paths' => array(
                         'Quantity' => $nPaths,
                         'Items' => $this->Paths,
                    ),
                    'CallerReference' => "{$this->CFID}{$this->epoch}",
            )));
            if (!empty($result['Invalidation']['Id'])) {
                echo "The following Invalidation was created: " .  $result['Invalidation']['Id'] . "\n";
                array_push($this->Invalidations, $result['Invalidation']['Id']);
            }
        }
    }

    public function checkInvalidations () 
    {
        $nInvalidations = count($this->Invalidations);
        if ($nInvalidations > 0) {
            foreach ($this->Invalidations as $invalidation) {
                $result = $this->CF->getInvalidation(array(
                    'DistributionId' => $this->CFID,
                    'Id' => $invalidation,
                ));
                if (!empty($result['Invalidation']['Status'])) {
                    if (CFController::STATUS_IN_PROGRESS == $result['Invalidation']['Status']) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}

$S3 = new S3Controller($argv, $argc);

